﻿<%@ Assembly Name="TAVIA.SharePoint.Club0803, Version=1.0.0.0, Culture=neutral, PublicKeyToken=e94667b04745ceb1" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisplayItem.aspx.cs" Inherits="TAVIA.SharePoint.Club0803.DiscountedItems.DisplayItem" MasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <style>
        #sideNavBox {
            display: none;
        }

        #contentBox {
            margin-right: 20px;
            margin-left: 20px;
        }

        .h4, h4 {
            font-size: 1.5rem;
        }
    </style>

    <SharePoint:UIVersionedContent ID="UIVersionedContent1" UIVersion="4" runat="server">
        <contenttemplate>
		<SharePoint:CssRegistration Name="forms.css" runat="server"/>
	</contenttemplate>
    </SharePoint:UIVersionedContent>

    <link href="/_layouts/15/club0803/styles/bootstrap.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-reboot.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/style.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <section class="col-md-12 form-container pb-3">
        <h2 class="title-container text-center py-3">Discounted Item</h2>
        <div class="form-bg">
            <!-- section2 -->
            <div class="row p-3">
                <div class="col-md-12">
                    <h5 class="text-blue">Admin Details</h5>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Full Name</label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblEmployeeName" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Email</label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblEmployeeEmailAddress" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <hr class="mtn-divider m-0 px-3" />
            </div>
            <!-- section3 -->
            <div class="row p-3">
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Category<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblCategory" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Type<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                        <asp:Label ID="lblCategoryType" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <hr class="mtn-divider m-0 px-3" />
            </div>
            <!-- section3 -->
            <div class="row p-3">
                <div class="col-md-12">
                    <h5 class="text-blue">Vendor Details</h5>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Vendor Name<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblVendorName" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Contact Person<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblVendorContactPerson" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Phone Number<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblVendorPhoneNumber" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Contact Email<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblVendorContactEmail" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Contact Address<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblVendorContactAddress" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <hr class="mtn-divider m-0 px-3" />
            </div>

            <div class="row p-3">
                <div class="col-md-12">
                    <h5 class="text-blue">Discounted Item Details</h5>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Product Name<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblProductName" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Description of the Product<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblProductDescription" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Vendor/Product Website URL<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:HyperLink ID="hlVendorProductWebsiteURL" runat="server" Target="_blank"></asp:HyperLink>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Closing Date<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                             <asp:Label ID="lblClosingDate" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Payment Options<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblPaymentOptions" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Status</label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblStatus" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Interest Rate<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblInterestRate" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Discount Rate<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblDiscountRate" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Any other important information<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblOtherImportantInformation" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Product Price</label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblProductPrice" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Upload Image<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:HyperLink ID="hlAttachment" runat="server"></asp:HyperLink>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <hr class="mtn-divider m-0 px-3" />
            </div>
            <div class="col-md-12 py-3">
                <div class="row py-2">
                        <div class="col-md-3 col-sm-3 col-12">
                            <label>Comment</label>
                        </div>
                        <div class="col-md-4 col-sm-4 col-12">
                            <asp:TextBox ID="txtComment" runat="server" Rows="3" TextMode="MultiLine" class="form-control mtn-input mtn-textbox" Visible="false"></asp:TextBox>
                            <SharePoint:InputFormRequiredFieldValidator ID="rfvComment" class="ms-error" Visible="false"
                                ControlToValidate="txtComment" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" BreakBefore="false" ValidationGroup="RejectVG">
                            </SharePoint:InputFormRequiredFieldValidator>
                            <asp:Label ID="lblComment" Text="&nbsp;" runat="server" Visible="false"></asp:Label>
                        </div>
                 </div>
            </div>
            <!-- button section -->
            <div class="col-md-12 py-3">
                <div class="row btn-div text-center">
                    <asp:Button ID="btnApproveAndPublish" class="btn mtn-btn mr-3" runat="server" Text="Approve & Publish" OnClick="btnApproveAndPublish_Click" Visible="false" />
                    <asp:Button ID="btnApproveAndDontPublish" class="btn mtn-btn mr-3" runat="server" Text="Approve & Do Not Publish" OnClick="btnApproveAndDontPublish_Click" Visible="false" />
                    <asp:Button ID="btnReject" class="btn mtn-btn mr-3" runat="server" Text="Reject" OnClick="btnReject_Click" Visible="false" ValidationGroup="RejectVG"/>
                    <asp:Button ID="btnClose" class="btn mtn-btn mr-3" CausesValidation="false" runat="server" Text="Close" OnClick="btnClose_Click" />
                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Club0803 - View Discounted Item
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" />

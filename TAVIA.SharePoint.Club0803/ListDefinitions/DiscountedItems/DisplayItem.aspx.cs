﻿using System;
using System.Collections.Specialized;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using TAVIA.SharePoint.Club0803.Utility;
using TAVIA.SharePoint.Utilities;

namespace TAVIA.SharePoint.Club0803.DiscountedItems
{
    public partial class DisplayItem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
            if (ribbon != null)
            {
                ribbon.Visible = false;
            }
            SPListItem CurrentItem = SPContext.Current.ListItem;
            //if (CurrentItem["Title"] != null)
            //{
            //    if (CurrentItem["Title"].ToString() == "Draft")
            //    {
            //        SPFieldUserValueCollection employeeUserValueCollection = new SPFieldUserValueCollection(SPContext.Current.Web, CurrentItem["Author"].ToString());
            //        SPUser employeeUser = employeeUserValueCollection[0].User;
            //        if (SPContext.Current.Web.CurrentUser.ID == employeeUser.ID)
            //        {
            //            string url = CurrentItem.ParentList.ParentWeb.Site.MakeFullUrl(CurrentItem.ParentList.DefaultNewFormUrl) + "?ID=" + CurrentItem.ID;
            //            Response.Redirect(url);
            //        }
            //    }
            //}
            //displaySCMSourcingRequestUC.CurrentItem = CurrentItem;
            DisplayDetails(CurrentItem);
        }

        private void DisplayDetails(SPListItem currentItem)
        {
            if (currentItem != null)
            {
                if (currentItem["Title"] != null)
                {
                    lblProductName.Text = currentItem["Title"].ToString();
                }

                //if (currentItem["RequestNumber"] != null)
                //{
                //    rowRequestNumber.Visible = true;
                //    lblRequestNumber.Text = currentItem["RequestNumber"].ToString();
                //}

                SPFieldUserValueCollection employeeUserValueCollection = new SPFieldUserValueCollection(SPContext.Current.Web, currentItem["Author"].ToString());
                SPUser employeeUser = employeeUserValueCollection[0].User;
                //if (currentItem.Fields.ContainsField("Status"))
                //{
                //    if (currentItem["Status"] != null)
                //    {
                //        SPFieldWorkflowStatus statusField = currentItem.Fields.GetField("Status") as SPFieldWorkflowStatus;
                //        object statusValue = currentItem["Status"];
                //        if (statusValue != null)
                //        {
                //            string rootWebUrl = SPContext.Current.Site.RootWeb.Url;
                //            lblStatus.Text = statusField.GetFieldValueAsText(statusValue);
                //            trStatus.Visible = true;
                //        }
                //    }
                //}

                lblEmployeeName.Text = employeeUser.Name;
                lblEmployeeEmailAddress.Text = employeeUser.Email;

                if (currentItem["Category"] != null)
                {
                    SPFieldLookupValue category = new SPFieldLookupValue(currentItem["Category"].ToString());
                    lblCategory.Text = category.LookupValue;
                }
                if (currentItem["CategoryType"] != null)
                {
                    SPFieldLookupValue categoryType = new SPFieldLookupValue(currentItem["CategoryType"].ToString());
                    lblCategoryType.Text = categoryType.LookupValue;
                }
                if (currentItem["VendorName"] != null)
                {
                    SPFieldLookupValue vendorName = new SPFieldLookupValue(currentItem["VendorName"].ToString());
                    lblVendorName.Text = vendorName.LookupValue;
                    FillVendorDetails(vendorName.LookupId);
                }
                if (currentItem["ProductDescription"] != null)
                {
                    lblProductDescription.Text = currentItem["ProductDescription"].ToString().Replace("\r\n", "<br />");
                }
                if (currentItem["VendorProductWebsiteURL"] != null)
                {
                    hlVendorProductWebsiteURL.NavigateUrl = currentItem["VendorProductWebsiteURL"].ToString();
                    hlVendorProductWebsiteURL.Text = currentItem["VendorProductWebsiteURL"].ToString();
                }
                if (currentItem["ClosingDate"] != null)
                    lblClosingDate.Text = ((DateTime)currentItem["ClosingDate"]).ToString(ConfigHandler.DateFormat());

                if (currentItem["PaymentOptions"] != null)
                    lblPaymentOptions.Text = currentItem["PaymentOptions"].ToString();
                if (currentItem["InterestRate"] != null)
                    lblInterestRate.Text = currentItem["InterestRate"].ToString();
                if (currentItem["DiscountRate"] != null)
                    lblDiscountRate.Text = currentItem["DiscountRate"].ToString();

                if (currentItem["OtherImportantInformation"] != null)
                {
                    lblOtherImportantInformation.Text = currentItem["OtherImportantInformation"].ToString().Replace("\r\n", "<br />");
                }
                if (currentItem["ProductPrice"] != null)
                    lblProductPrice.Text = currentItem["ProductPrice"].ToString();

                if (currentItem.Attachments.Count > 0)
                {
                    hlAttachment.NavigateUrl = currentItem.Attachments.UrlPrefix + currentItem.Attachments[0];
                    hlAttachment.Text = currentItem.Attachments[0];
                }
                else
                {
                    hlAttachment.Visible = false;
                }

                if (currentItem["Status"] != null)
                {
                    string status = currentItem["Status"].ToString();
                    lblStatus.Text = status;

                    if (status == "Pending Approval" || status == "Approved and Not Published")
                    {
                        string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);

                        if (SPContext.Current.Web.SiteGroups[SuperAdminsGroupName].ContainsCurrentUser)
                        {
                            txtComment.Visible = true;
                            rfvComment.Visible = true;
                            if (status == "Pending Approval")
                            {
                                btnApproveAndPublish.Visible = true;
                                btnApproveAndDontPublish.Visible = true;
                                btnReject.Visible = true;
                            }
                            else if (status == "Approved and Not Published")
                            {
                                btnApproveAndPublish.Visible = true;
                                btnApproveAndPublish.Text = "Publish";
                            }
                        }
                    }
                    else
                    {
                        lblComment.Visible = true;
                        if (currentItem["Comments"] != null)
                            lblComment.Text = currentItem["Comments"].ToString();
                    }
                }


                //if (CurrentItem["Comments"] != null)
                //{
                //    string commentsStr = CurrentItem["Comments"].ToString();
                //    if (!String.IsNullOrEmpty(commentsStr))
                //    {
                //        List<ActorComment> comments = (new System.Web.Script.Serialization.JavaScriptSerializer()).Deserialize<List<ActorComment>>(commentsStr);
                //        grdComments.DataSource = comments;
                //        grdComments.DataBind();
                //    }
                //    else
                //    {
                //        trNoComments.Visible = true;
                //    }
                //}
                //else
                //{
                //    trNoComments.Visible = true;
                //}


            }
        }

        private void FillVendorDetails(int vendorID)
        {
            string VendorsListInternalName = SPUtility.GetLocalizedString("$Resources:VendorsListUrl", "Club0803_Config", 1033);
            string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, VendorsListInternalName);
            SPList VendorsList = SPContext.Current.Web.GetList(listUrl);
            SPListItem vendorItem = VendorsList.GetItemById(vendorID);

            if (vendorItem["PhoneNumber"] != null)
            {
                lblVendorPhoneNumber.Text = vendorItem["PhoneNumber"].ToString();
            }

            if (vendorItem["Email"] != null)
            {
                lblVendorContactEmail.Text = vendorItem["Email"].ToString();
            }

            if (vendorItem["ContactPerson"] != null)
            {
                lblVendorContactPerson.Text = vendorItem["ContactPerson"].ToString();
            }

            if (vendorItem["ContactAddress"] != null)
            {
                lblVendorContactAddress.Text = vendorItem["ContactAddress"].ToString().Replace("\r\n", "<br />"); ;
            }
        }

        protected void btnApproveAndPublish_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            SPListItem item = SPContext.Current.ListItem;

            item["Status"] = "Approved and Published";
            if (!String.IsNullOrEmpty(txtComment.Text))
                item["Comments"] = txtComment.Text;
            else
                item["Comments"] = String.Empty;

            item.Update();

            SendDiscountedItemApprovedNotificationToAdmin(item, "Approved and Published");
            SendDiscountedItemApprovedNotificationToMembers(item);

            GoBack();
        }
        protected void btnApproveAndDontPublish_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            SPListItem item = SPContext.Current.ListItem;

            item["Status"] = "Approved and Not Published";
            if (!String.IsNullOrEmpty(txtComment.Text))
                item["Comments"] = txtComment.Text;
            else
                item["Comments"] = String.Empty;

            item.Update();

            SendDiscountedItemApprovedNotificationToAdmin(item, "Approved and Not Published");

            GoBack();
        }
        protected void btnReject_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid)
                return;

            SPListItem item = SPContext.Current.ListItem;

            item["Status"] = "Rejected";
            if (!String.IsNullOrEmpty(txtComment.Text))
                item["Comments"] = txtComment.Text;
            else
                item["Comments"] = String.Empty;

            item.Update();

            SendDiscountedItemApprovedNotificationToAdmin(item, "Rejected");

            GoBack();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        private void GoBack()
        {
            if (Page.Request.QueryString["IsDlg"] != null)
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                context.Response.Write("<script type='text/javascript'>window.frameElement.cancelPopUp()</script>");
                context.Response.Flush();
                context.Response.End();
            }
            else if (!String.IsNullOrEmpty(Page.Request.QueryString["Source"]))
            {
                Response.Redirect(Page.Request.QueryString["Source"].ToString());
            }
            else
            {
                string DiscountedItemsListInternalName = SPUtility.GetLocalizedString("$Resources:DiscountedItemsListUrl", "Club0803_Config", 1033);
                string url = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, DiscountedItemsListInternalName);
                Response.Redirect(url);
                //Response.Redirect(SPContext.Current.Web.Url);
            }
        }

        private void SendDiscountedItemApprovedNotificationToAdmin(SPListItem discountedItem, string status)
        {
            string senderEmailSignature = SPUtility.GetLocalizedString("$Resources:SenderEmailSignature", "Club0803_Config", 1033);
            string senderEmailAddress = SPUtility.GetLocalizedString("$Resources:SenderEmailAddress", "Club0803_Config", 1033);
            string superAdminsGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupEmailAddress", "Club0803_Config", 1033);
            string adminsGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:AdminsGroupEmailAddress", "Club0803_Config", 1033);

            //string TribesListInternalName = SPUtility.GetLocalizedString("$Resources:Lists_Tribes_ListUrl", "InnovationPortal_Config", 1033);
            //string TribesListUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, TribesListInternalName);
            //SPList TribesList = impersonatedWeb.GetList(TribesListUrl);
            //SPListItem tribeItem = TribesList.GetItemById(tribeID);
            //String tribeName = tribeItem["Title"] != null ? tribeItem["Title"].ToString() : string.Empty;
            //SPFieldUserValueCollection createdByUserValueCollection = new SPFieldUserValueCollection(impersonatedWeb, tribeItem["Author"].ToString());
            //SPUser createdByUser = createdByUserValueCollection[0].User;
            //String createdBy = createdByUser.Name;
            //String createdDate = ((DateTime)tribeItem["Created"]).ToString("dd MMM yyyy");
            DateTime ExpirationDate = ((DateTime)discountedItem["ClosingDate"]);
            string productName = discountedItem["Title"] != null ? discountedItem["Title"].ToString() : string.Empty;
            string productCategory = String.Empty;
            if (discountedItem["Category"] != null)
            {
                SPFieldLookupValue category = new SPFieldLookupValue(discountedItem["Category"].ToString());
                productCategory = category.LookupValue;
            }

            ListDictionary lstReplacements = new ListDictionary();
            lstReplacements.Add("<%" + EmailReplacementKeys.DiscountedItemNumber.ToString() + "%>", discountedItem.ID.ToString());
            lstReplacements.Add("<%" + EmailReplacementKeys.Action.ToString() + "%>", status);
            lstReplacements.Add("<%" + EmailReplacementKeys.DiscountedItemCategory.ToString() + "%>", productCategory);
            lstReplacements.Add("<%" + EmailReplacementKeys.ExpirationDate.ToString() + "%>", ExpirationDate.ToString("dd MMM yyyy"));
            lstReplacements.Add("<%" + EmailReplacementKeys.ProductName.ToString() + "%>", productName);
            string discountedItemUrl = discountedItem.ParentList.ParentWeb.Site.MakeFullUrl(discountedItem.ParentList.DefaultDisplayFormUrl) + "?ID=" + discountedItem.ID.ToString();
            lstReplacements.Add("<%" + EmailReplacementKeys.DiscountedItemUrl.ToString() + "%>", "<a href='" + discountedItemUrl + "'>click here</a>");
            lstReplacements.Add("<%" + EmailReplacementKeys.SenderEmailSignature.ToString() + "%>", senderEmailSignature);

            string subjectToAdmin = SPUtility.GetLocalizedString("$Resources:DiscountedItemApprovedNotificationToAdminEmailSubject", "Club0803_Config", 1033);
            string bodyToAdmin = SPUtility.GetLocalizedString("$Resources:DiscountedItemApprovedNotificationToAdminEmailBody", "Club0803_Config", 1033);

            string subjectExpirationToAdmin = SPUtility.GetLocalizedString("$Resources:DiscountedItemExpirationNotificationToAdminEmailSubject", "Club0803_Config", 1033);
            string bodyExpirationToAdmin = SPUtility.GetLocalizedString("$Resources:DiscountedItemExpirationNotificationToAdminEmailBody", "Club0803_Config", 1033);

            EMailHandler.SendEmail(subjectToAdmin, bodyToAdmin, adminsGroupEmailAddress, ccList: superAdminsGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
            EMailHandler.AddReminder(subjectExpirationToAdmin, bodyExpirationToAdmin, ExpirationDate.AddDays(-1), discountedItem.UniqueId, adminsGroupEmailAddress, ccList: superAdminsGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
            EMailHandler.AddReminder(subjectExpirationToAdmin, bodyExpirationToAdmin, ExpirationDate.AddDays(-3), discountedItem.UniqueId, adminsGroupEmailAddress, ccList: superAdminsGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
            EMailHandler.AddReminder(subjectExpirationToAdmin, bodyExpirationToAdmin, ExpirationDate.AddDays(-7), discountedItem.UniqueId, adminsGroupEmailAddress, ccList: superAdminsGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
        }

        private void SendDiscountedItemApprovedNotificationToMembers(SPListItem discountedItem)
        {
            string senderEmailSignature = SPUtility.GetLocalizedString("$Resources:SenderEmailSignature", "Club0803_Config", 1033);
            string senderEmailAddress = SPUtility.GetLocalizedString("$Resources:SenderEmailAddress", "Club0803_Config", 1033);
            string permanentStaffGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:PermanentStaffGroupEmailAddress", "Club0803_Config", 1033);

            DateTime ExpirationDate = ((DateTime)discountedItem["ClosingDate"]);
            string productName = discountedItem["Title"] != null ? discountedItem["Title"].ToString() : string.Empty;
            string productDescription = discountedItem["ProductDescription"] != null ? discountedItem["ProductDescription"].ToString().Replace("\r\n", "<br />") : string.Empty;
            string productCategory = String.Empty;
            if (discountedItem["Category"] != null)
            {
                SPFieldLookupValue category = new SPFieldLookupValue(discountedItem["Category"].ToString());
                productCategory = category.LookupValue;
            }

            ListDictionary lstReplacements = new ListDictionary();
            lstReplacements.Add("<%" + EmailReplacementKeys.DiscountedItemCategory.ToString() + "%>", productCategory);
            lstReplacements.Add("<%" + EmailReplacementKeys.ExpirationDate.ToString() + "%>", ExpirationDate.ToString("dd MMM yyyy"));
            lstReplacements.Add("<%" + EmailReplacementKeys.ProductName.ToString() + "%>", productName);
            lstReplacements.Add("<%" + EmailReplacementKeys.ProductDescription.ToString() + "%>", productDescription);
            string discountedItemUrl = SPContext.Current.Web.Url + "/Pages/ViewDiscountedItem.aspx" + "?ID=" + discountedItem.ID.ToString();
            lstReplacements.Add("<%" + EmailReplacementKeys.DiscountedItemUrl.ToString() + "%>", "<a href='" + discountedItemUrl + "'>click here</a>");
            lstReplacements.Add("<%" + EmailReplacementKeys.SenderEmailSignature.ToString() + "%>", senderEmailSignature);

            string subjectToMembers = SPUtility.GetLocalizedString("$Resources:DiscountedItemApprovedNotificationToMembersEmailSubject", "Club0803_Config", 1033);
            string bodyToMembers = SPUtility.GetLocalizedString("$Resources:DiscountedItemApprovedNotificationToMembersEmailBody", "Club0803_Config", 1033);

            string subjectExpirationToMembers = SPUtility.GetLocalizedString("$Resources:DiscountedItemExpirationNotificationToMembersEmailSubject", "Club0803_Config", 1033);
            string bodyExpirationToMembers = SPUtility.GetLocalizedString("$Resources:DiscountedItemExpirationNotificationToMembersEmailBody", "Club0803_Config", 1033);

            EMailHandler.SendEmail(subjectToMembers, bodyToMembers, permanentStaffGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
            EMailHandler.AddReminder(subjectExpirationToMembers, bodyExpirationToMembers, ExpirationDate.AddDays(-1), discountedItem.UniqueId, permanentStaffGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
            EMailHandler.AddReminder(subjectExpirationToMembers, bodyExpirationToMembers, ExpirationDate.AddDays(-3), discountedItem.UniqueId, permanentStaffGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
            EMailHandler.AddReminder(subjectExpirationToMembers, bodyExpirationToMembers, ExpirationDate.AddDays(-7), discountedItem.UniqueId, permanentStaffGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
            //DiscountedItemApprovedNotificationToMembersSMS
        }

    }
}

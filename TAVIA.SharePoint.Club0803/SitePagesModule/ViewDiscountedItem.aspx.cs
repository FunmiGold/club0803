﻿using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;
using TAVIA.SharePoint.Utilities;

namespace TAVIA.SharePoint.Club0803.SitePagesModule
{
    public partial class ViewDiscountedItem : WebPartPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
                if (ribbon != null)
                {
                    ribbon.Visible = false;
                }
                bool isValid = DisplayDetails();
                if (!isValid)
                {
                    //Add code to redirect to error page
                    Response.Redirect("Info.aspx?msg=" + (int)Utility.Messages.Disocunted_Item_Does_Not_Exist_Or_Expired);
                }
            }
        }

        private bool DisplayDetails()
        {
            bool isValid = false;
            if (!String.IsNullOrEmpty(Page.Request.QueryString["ID"]))
            {
                int itemId;
                if (int.TryParse(Page.Request.QueryString["ID"].ToString(), out itemId))
                {
                    string DiscountedItemsListInternalName = SPUtility.GetLocalizedString("$Resources:DiscountedItemsListUrl", "Club0803_Config", 1033);
                    string DiscountedItemsListUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, DiscountedItemsListInternalName);
                    SPList DiscountedItemsList = SPContext.Current.Web.GetList(DiscountedItemsListUrl);
                    SPListItem currentItem = null;
                    try
                    {
                        currentItem = DiscountedItemsList.GetItemById(itemId);
                    }
                    catch (Exception ex)
                    {
                        //Add code to redirect to error page
                        Response.Redirect("Info.aspx?msg=" + (int)Utility.Messages.Disocunted_Item_Does_Not_Exist_Or_Expired);
                    }
                    if (currentItem != null)
                    {
                        if (currentItem["Title"] != null)
                        {
                            lblProductName.Text = currentItem["Title"].ToString();
                        }

                        if (currentItem["Category"] != null)
                        {
                            SPFieldLookupValue category = new SPFieldLookupValue(currentItem["Category"].ToString());
                            lblCategory.Text = category.LookupValue;
                        }
                        if (currentItem["CategoryType"] != null)
                        {
                            SPFieldLookupValue categoryType = new SPFieldLookupValue(currentItem["CategoryType"].ToString());
                            lblCategoryType.Text = categoryType.LookupValue;
                        }
                        if (currentItem["VendorName"] != null)
                        {
                            SPFieldLookupValue vendorName = new SPFieldLookupValue(currentItem["VendorName"].ToString());
                            lblVendorName.Text = vendorName.LookupValue;
                            FillVendorDetails(vendorName.LookupId);
                        }
                        if (currentItem["ProductDescription"] != null)
                        {
                            lblProductDescription.Text = currentItem["ProductDescription"].ToString().Replace("\r\n", "<br />");
                        }
                        if (currentItem["VendorProductWebsiteURL"] != null)
                        {
                            hlVendorProductWebsiteURL.NavigateUrl = currentItem["VendorProductWebsiteURL"].ToString();
                            hlVendorProductWebsiteURL.Text = currentItem["VendorProductWebsiteURL"].ToString();
                        }
                        DateTime closingDate = DateTime.Now.AddDays(-1);
                        if (currentItem["ClosingDate"] != null)
                        {
                            closingDate = (DateTime)currentItem["ClosingDate"];
                            lblClosingDate.Text = closingDate.ToString(ConfigHandler.DateFormat());
                        }

                        if (currentItem["PaymentOptions"] != null)
                            lblPaymentOptions.Text = currentItem["PaymentOptions"].ToString();
                        if (currentItem["InterestRate"] != null)
                            lblInterestRate.Text = currentItem["InterestRate"].ToString();
                        if (currentItem["DiscountRate"] != null)
                            lblDiscountRate.Text = currentItem["DiscountRate"].ToString();

                        if (currentItem["OtherImportantInformation"] != null)
                        {
                            lblOtherImportantInformation.Text = currentItem["OtherImportantInformation"].ToString().Replace("\r\n", "<br />");
                        }
                        if (currentItem["ProductPrice"] != null)
                            lblProductPrice.Text = currentItem["ProductPrice"].ToString();

                        if (currentItem.Attachments.Count > 0)
                        {
                            imgDiscountedItem.Src = currentItem.Attachments.UrlPrefix + currentItem.Attachments[0];
                        }

                        if (currentItem["Status"] != null)
                        {
                            string status = currentItem["Status"].ToString();
                            int orderItemId = 0;
                            int orderCartItemId = 0;
                            SPUser currentUser = SPContext.Current.Web.CurrentUser;

                            SPSecurity.RunWithElevatedPrivileges(delegate ()
                            {
                                using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
                                {
                                    using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
                                    {
                                        impersonatedWeb.AllowUnsafeUpdates = true;

                                        string OrdersListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersListUrl", "Club0803_Config", 1033);
                                        string OrdersListUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, OrdersListInternalName);
                                        SPList OrdersList = impersonatedWeb.GetList(OrdersListUrl);

                                        SPQuery queryOrders = new SPQuery();
                                        queryOrders.Query = String.Format("<Where><And><Eq><FieldRef Name=\"EmployeeName\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq><Eq><FieldRef Name=\"DiscountedItem\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{1}</Value></Eq></And></Where>", currentUser.ID, itemId);
                                        queryOrders.ViewFields = "<FieldRef Name=\"ID\" /><FieldRef Name=\"DiscountedItem\" /><FieldRef Name=\"EmployeeName\" /><FieldRef Name=\"DiscountedItemRating\" /><FieldRef Name=\"VendorResponseRating\" /><FieldRef Name=\"VendorComplaintsRating\" />";
                                        queryOrders.ViewFieldsOnly = true;
                                        queryOrders.RowLimit = 1;

                                        SPListItemCollection colOrdersItems = OrdersList.GetItems(queryOrders);
                                        if (colOrdersItems.Count > 0)
                                        {
                                            SPListItem orderItem = colOrdersItems[0];
                                            orderItemId = orderItem.ID;
                                            btnSubmitRating.Visible = true;
                                            divRating1.Visible = true;
                                            divRating2.Visible = true;
                                            divRating3.Visible = true;
                                            if (orderItem["DiscountedItemRating"] != null)
                                            {
                                                rblDiscountedItemRating.Text = orderItem["DiscountedItemRating"].ToString();
                                            }
                                            if (orderItem["VendorResponseRating"] != null)
                                            {
                                                rblVendorResponseRating.Text = orderItem["VendorResponseRating"].ToString();
                                            }
                                            if (orderItem["VendorComplaintsRating"] != null)
                                            {
                                                rblVendorComplaintsRating.Text = orderItem["VendorComplaintsRating"].ToString();
                                            }
                                        }

                                        string OrdersCartListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersCartListUrl", "Club0803_Config", 1033);
                                        string OrdersCartListUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, OrdersCartListInternalName);
                                        SPList OrdersCartList = impersonatedWeb.GetList(OrdersCartListUrl);

                                        SPQuery queryOrdersCart = new SPQuery();
                                        queryOrdersCart.Query = String.Format("<Where><And><Eq><FieldRef Name=\"EmployeeName\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq><Eq><FieldRef Name=\"DiscountedItem\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{1}</Value></Eq></And></Where>", currentUser.ID, itemId);
                                        queryOrdersCart.ViewFields = "<FieldRef Name=\"ID\" /><FieldRef Name=\"DiscountedItem\" /><FieldRef Name=\"EmployeeName\" />";
                                        queryOrdersCart.ViewFieldsOnly = true;
                                        queryOrdersCart.RowLimit = 1;

                                        SPListItemCollection colOrdersCartItems = OrdersCartList.GetItems(queryOrdersCart);
                                        if (colOrdersCartItems.Count > 0)
                                        {
                                            orderCartItemId = colOrdersCartItems[0].ID;
                                        }

                                        impersonatedWeb.AllowUnsafeUpdates = false;
                                    }
                                }
                            });

                            if (orderItemId > 0)
                            {
                                lblStatus.Text = "Already Ordered";
                            }
                            else if (orderCartItemId > 0)
                            {
                                lblStatus.Text = "Already Added to Cart";
                                btnRemoveFromCart.Visible = true;
                            }
                            else if (status == "Approved and Published")
                            {
                                if (closingDate >= DateTime.Now)
                                {
                                    lblStatus.Text = "Available";
                                    btnAddToCart.Visible = true;
                                }
                                else
                                {
                                    lblStatus.Text = "Expired";
                                }
                            }
                            else
                            {
                                //Any other case like not approved
                                Response.Redirect("Info.aspx?msg=" + (int)Utility.Messages.Disocunted_Item_Does_Not_Exist_Or_Expired);
                            }
                            isValid = true;
                        }
                    }
                }
            }

            return isValid;
        }

        private void FillVendorDetails(int vendorID)
        {
            string VendorsListInternalName = SPUtility.GetLocalizedString("$Resources:VendorsListUrl", "Club0803_Config", 1033);
            string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, VendorsListInternalName);
            SPList VendorsList = SPContext.Current.Web.GetList(listUrl);
            SPListItem vendorItem = VendorsList.GetItemById(vendorID);

            if (vendorItem["PhoneNumber"] != null)
            {
                lblVendorPhoneNumber.Text = vendorItem["PhoneNumber"].ToString();
            }

            if (vendorItem["Email"] != null)
            {
                lblVendorContactEmail.Text = vendorItem["Email"].ToString();
            }

            if (vendorItem["ContactPerson"] != null)
            {
                lblVendorContactPerson.Text = vendorItem["ContactPerson"].ToString();
            }

            if (vendorItem["ContactAddress"] != null)
            {
                lblVendorContactAddress.Text = vendorItem["ContactAddress"].ToString().Replace("\r\n", "<br />"); ;
            }
        }

        protected void btnSubmitRating_Click(object sender, EventArgs e)
        {
            //if (!Page.IsValid)
            //    return;

            SPUser currentUser = SPContext.Current.Web.CurrentUser;

            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
                    {
                        impersonatedWeb.AllowUnsafeUpdates = true;

                        int itemId;
                        if (int.TryParse(Page.Request.QueryString["ID"].ToString(), out itemId))
                        {
                            string OrdersListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersListUrl", "Club0803_Config", 1033);
                            string OrdersListUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, OrdersListInternalName);
                            SPList OrdersList = impersonatedWeb.GetList(OrdersListUrl);

                            SPQuery queryOrders = new SPQuery();
                            queryOrders.Query = String.Format("<Where><And><Eq><FieldRef Name=\"EmployeeName\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq><Eq><FieldRef Name=\"DiscountedItem\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{1}</Value></Eq></And></Where>", currentUser.ID, itemId);
                            //queryOrders.ViewFields = "<FieldRef Name=\"ID\" /><FieldRef Name=\"DiscountedItem\" /><FieldRef Name=\"EmployeeName\" /><FieldRef Name=\"DiscountedItemRating\" /><FieldRef Name=\"VendorResponseRating\" /><FieldRef Name=\"VendorComplaintsRating\" />";
                            //queryOrders.ViewFieldsOnly = true;
                            queryOrders.RowLimit = 1;

                            SPListItemCollection colOrdersItems = OrdersList.GetItems(queryOrders);
                            if (colOrdersItems.Count > 0)
                            {
                                SPListItem orderItem = colOrdersItems[0];
                                if (!String.IsNullOrEmpty(rblDiscountedItemRating.Text))
                                {
                                    orderItem["DiscountedItemRating"] = int.Parse(rblDiscountedItemRating.Text);
                                }
                                else
                                {
                                    orderItem["DiscountedItemRating"] = null;
                                }
                                if (!String.IsNullOrEmpty(rblVendorResponseRating.Text))
                                {
                                    orderItem["VendorResponseRating"] = int.Parse(rblVendorResponseRating.Text);
                                }
                                else
                                {
                                    orderItem["VendorResponseRating"] = null;
                                }
                                if (!String.IsNullOrEmpty(rblVendorComplaintsRating.Text))
                                {
                                    orderItem["VendorComplaintsRating"] = int.Parse(rblVendorComplaintsRating.Text);
                                }
                                else
                                {
                                    orderItem["VendorComplaintsRating"] = null;
                                }
                                orderItem.Update();
                            }

                        }

                        impersonatedWeb.AllowUnsafeUpdates = false;
                    }
                }
            });

            //GoBack();
            Response.Redirect("Info.aspx?msg=" + (int)Utility.Messages.Rating_Submitted_Successfully);
        }

        protected void btnAddToCart_Click(object sender, EventArgs e)
        {
            //if (!Page.IsValid)
            //    return;

            SPUser currentUser = SPContext.Current.Web.CurrentUser;

            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
                    {
                        impersonatedWeb.AllowUnsafeUpdates = true;

                        //string OrdersListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersListUrl", "Club0803_Config", 1033);
                        //string OrdersListUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, OrdersListInternalName);
                        //SPList OrdersList = impersonatedWeb.GetList(OrdersListUrl);

                        int itemId;
                        if (int.TryParse(Page.Request.QueryString["ID"].ToString(), out itemId))
                        {
                            string DiscountedItemsListInternalName = SPUtility.GetLocalizedString("$Resources:DiscountedItemsListUrl", "Club0803_Config", 1033);
                            string DiscountedItemsListUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, DiscountedItemsListInternalName);
                            SPList DiscountedItemsList = SPContext.Current.Web.GetList(DiscountedItemsListUrl);
                            SPListItem currentItem = DiscountedItemsList.GetItemById(itemId);

                            if (currentItem != null)
                            {
                                string OrdersCartListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersCartListUrl", "Club0803_Config", 1033);
                                string OrdersCartListUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, OrdersCartListInternalName);
                                SPList OrdersCartList = impersonatedWeb.GetList(OrdersCartListUrl);

                                SPListItem cartItem = OrdersCartList.AddItem();

                                SPFieldUserValue UserNameUserValue = new SPFieldUserValue(impersonatedWeb, currentUser.ID, currentUser.LoginName);

                                cartItem["DiscountedItem"] = new SPFieldLookupValue(currentItem.ID, currentItem.Title); ;
                                cartItem["EmployeeName"] = UserNameUserValue;
                                cartItem["DiscountedItemID"] = currentItem.ID;
                                SPFieldLookupValue vendorName = new SPFieldLookupValue(currentItem["VendorName"].ToString());
                                cartItem["Vendor"] = vendorName.LookupValue;
                                if (currentItem.Attachments.Count > 0)
                                {
                                    cartItem["ImgUrl"] = currentItem.Attachments.UrlPrefix + currentItem.Attachments[0];
                                }
                                else
                                    cartItem["ImgUrl"] = "";

                                cartItem.Update();
                            }
                        }

                        impersonatedWeb.AllowUnsafeUpdates = false;
                    }
                }
            });

            GoBack();
        }
        protected void btnRemoveFromCart_Click(object sender, EventArgs e)
        {
            //if (!Page.IsValid)
            //    return;

            SPUser currentUser = SPContext.Current.Web.CurrentUser;

            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
                    {
                        impersonatedWeb.AllowUnsafeUpdates = true;

                        int itemId;
                        if (int.TryParse(Page.Request.QueryString["ID"].ToString(), out itemId))
                        {
                            string OrdersCartListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersCartListUrl", "Club0803_Config", 1033);
                            string OrdersCartListUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, OrdersCartListInternalName);
                            SPList OrdersCartList = impersonatedWeb.GetList(OrdersCartListUrl);

                            SPQuery queryOrdersCart = new SPQuery();
                            queryOrdersCart.Query = String.Format("<Where><And><Eq><FieldRef Name=\"EmployeeName\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq><Eq><FieldRef Name=\"DiscountedItem\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{1}</Value></Eq></And></Where>", currentUser.ID, itemId);
                            queryOrdersCart.ViewFields = "<FieldRef Name=\"ID\" /><FieldRef Name=\"DiscountedItem\" /><FieldRef Name=\"EmployeeName\" />";
                            queryOrdersCart.ViewFieldsOnly = true;
                            queryOrdersCart.RowLimit = 20;

                            SPListItemCollection colOrdersCartItems = OrdersCartList.GetItems(queryOrdersCart);
                            int itemCount = colOrdersCartItems.Count;
                            for (int i = 0; i < itemCount; i++)
                            {
                                colOrdersCartItems[0].Delete();
                            }
                        }

                        impersonatedWeb.AllowUnsafeUpdates = false;
                    }
                }
            });

            GoBack();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        private void GoBack()
        {
            if (Page.Request.QueryString["IsDlg"] != null)
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                context.Response.Write("<script type='text/javascript'>window.frameElement.cancelPopUp()</script>");
                context.Response.Flush();
                context.Response.End();
            }
            else if (!String.IsNullOrEmpty(Page.Request.QueryString["Source"]))
            {
                Response.Redirect(Page.Request.QueryString["Source"].ToString());
            }
            else
            {
                Response.Redirect("ViewDiscountedItems.aspx");
            }
        }


        //void FillDiscountedItems()
        //{

        //    SPUser currentUser = SPContext.Current.Web.CurrentUser;

        //    SPSecurity.RunWithElevatedPrivileges(delegate ()
        //    {
        //        using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
        //        {
        //            using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
        //            {
        //                impersonatedWeb.AllowUnsafeUpdates = true;

        //                int vendorId = int.Parse(ddlVendor.Text);
        //                int categoryId = int.Parse(ddlCategory.Text);

        //                string DiscountedItemsListInternalName = SPUtility.GetLocalizedString("$Resources:DiscountedItemsListUrl", "Club0803_Config", 1033);
        //                string listUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, DiscountedItemsListInternalName);
        //                SPList DiscountedItemsList = impersonatedWeb.GetList(listUrl);

        //                StringBuilder strQuery = new StringBuilder("<OrderBy><FieldRef Name=\"ClosingDate\" Ascending=\"FALSE\" /></OrderBy>");
        //                strQuery.Append("<Where><And><And><And>");
        //                strQuery.Append("<Eq><FieldRef Name=\"Status\" /><Value Type=\"Text\">Approved and Published</Value></Eq>");
        //                strQuery.Append("<Geq><FieldRef Name=\"ClosingDate\" /><Value Type=\"DateTime\"><Today /></Value></Geq></And>");
        //                if (vendorId > 0)
        //                    strQuery.Append(String.Format("<Eq><FieldRef Name=\"VendorName\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq></And>", vendorId));
        //                else
        //                    strQuery.Append(String.Format("<Neq><FieldRef Name=\"VendorName\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Neq></And>", vendorId));
        //                if (categoryId > 0)
        //                    strQuery.Append(String.Format("<Eq><FieldRef Name=\"Category\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq></And>", categoryId));
        //                else
        //                    strQuery.Append(String.Format("<Neq><FieldRef Name=\"Category\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Neq></And>", categoryId));
        //                strQuery.Append("</Where>");

        //                //<OrderBy><FieldRef Name="ClosingDate" Ascending="FALSE" /></OrderBy>
        //                //<Where><And><And><And><Eq><FieldRef Name="Status" /><Value Type="Text">Approved and Published</Value></Eq>
        //                //<Geq><FieldRef Name="ClosingDate" /><Value Type="DateTime"><Today /></Value></Geq></And>
        //                //<Eq><FieldRef Name="VendorName" /><Value Type="Text">1</Value></Eq></And>
        //                //<Neq><FieldRef Name="Category" /><Value Type="Text">2</Value></Neq></And>
        //                //</Where>



        //                //"<OrderBy><FieldRef Name=\"ClosingDate\" Ascending=\"FALSE\" /></OrderBy>
        //                //<Where><And><Eq><FieldRef Name=\"Status\" /><Value Type=\"Text\">Approved and Published</Value></Eq>
        //                //<Geq><FieldRef Name=\"ClosingDate\" /><Value Type=\"DateTime\"><Today /></Value></Geq></And>
        //                //</Where>";




        //                SPQuery query = new SPQuery();
        //                // query.Query = "<OrderBy><FieldRef Name=\"ClosingDate\" Ascending=\"FALSE\" /></OrderBy><Where><And><Eq><FieldRef Name=\"Status\" /><Value Type=\"Text\">Approved and Published</Value></Eq><Geq><FieldRef Name=\"ClosingDate\" /><Value Type=\"DateTime\"><Today /></Value></Geq></And></Where>";
        //                query.Query = strQuery.ToString();
        //                query.ViewFields = "<FieldRef Name=\"ID\" /><FieldRef Name=\"Title\" /><FieldRef Name=\"Category\" /><FieldRef Name=\"VendorName\" /><FieldRef Name=\"ClosingDate\" /><FieldRef Name=\"Status\" /><FieldRef Name=\"Attachments\" />";
        //                query.ViewFieldsOnly = true;
        //                //query.IncludeAttachmentUrls = true;
        //                query.RowLimit = 1000;

        //                SPListItemCollection colAllItems = DiscountedItemsList.GetItems(query);

        //                DataTable tblLatestItems = GetTableDefinition();
        //                DataTable tblAllItems = GetTableDefinition();

        //                //DataTable tblAllItems = DiscountedItemsList.GetItems(query).GetDataTable();

        //                foreach (SPListItem item in colAllItems)
        //                {
        //                    DataRow row = tblAllItems.NewRow();

        //                    row["ID"] = item.ID;
        //                    row["Title"] = item.Title;
        //                    row["ClosingDate"] = ((DateTime)item["ClosingDate"]).ToString("dd MMM yyyy");
        //                    if (item.Attachments.Count > 0)
        //                    {
        //                        row["ImgUrl"] = item.Attachments.UrlPrefix + item.Attachments[0];
        //                    }
        //                    else
        //                        row["ImgUrl"] = "";

        //                    tblAllItems.Rows.Add(row);
        //                }

        //                int maxCount = 4;
        //                if (colAllItems.Count < 4)
        //                    maxCount = colAllItems.Count;
        //                for (int i = 0; i < maxCount; i++)
        //                {
        //                    DataRow row = tblLatestItems.NewRow();

        //                    SPListItem item = colAllItems[i];
        //                    row["ID"] = item.ID;
        //                    row["Title"] = item.Title;
        //                    row["ClosingDate"] = ((DateTime)item["ClosingDate"]).ToString("dd MMM yyyy");
        //                    if (item.Attachments.Count > 0)
        //                    {
        //                        row["ImgUrl"] = item.Attachments.UrlPrefix + item.Attachments[0];
        //                    }
        //                    else
        //                        row["ImgUrl"] = "";

        //                    tblLatestItems.Rows.Add(row);
        //                }

        //                repLatestDiscountedItems.DataSource = tblLatestItems;
        //                repLatestDiscountedItems.DataBind();

        //                repDiscountedItems.DataSource = tblAllItems;
        //                repDiscountedItems.DataBind();

        //                impersonatedWeb.AllowUnsafeUpdates = false;
        //            }
        //        }
        //    });
        //}

        //private DataTable GetTableDefinition()
        //{
        //    DataTable tempTable = new DataTable();

        //    tempTable.Columns.Add("ID");
        //    tempTable.Columns.Add("Title");
        //    tempTable.Columns.Add("ClosingDate");
        //    tempTable.Columns.Add("ImgUrl");
        //    //tempTable.Columns.Add("");

        //    //query.ViewFields = "<FieldRef Name=\"ID\" />
        //    //<FieldRef Name=\"Title\" />
        //    //<FieldRef Name=\"Category\" />
        //    //<FieldRef Name=\"VendorName\" />
        //    //<FieldRef Name=\"ClosingDate\" />
        //    //<FieldRef Name=\"Status\" />
        //    //<FieldRef Name=\"Attachments\" />";

        //    return tempTable;
        //}

        //private void FillCategories()
        //{
        //    string CategoriesListInternalName = SPUtility.GetLocalizedString("$Resources:CategoriesListUrl", "Club0803_Config", 1033);
        //    string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, CategoriesListInternalName);
        //    SPList CategoriesList = SPContext.Current.Web.GetList(listUrl);
        //    ddlCategory.DataSource = CategoriesList.Items.GetDataTable();
        //    ddlCategory.DataValueField = "ID";
        //    ddlCategory.DataTextField = "Title";
        //    ddlCategory.DataBind();
        //    ddlCategory.Items.Insert(0, new ListItem("All Categories", "0"));
        //}

        //private void FillVendors()
        //{
        //    string VendorsListInternalName = SPUtility.GetLocalizedString("$Resources:VendorsListUrl", "Club0803_Config", 1033);
        //    string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, VendorsListInternalName);
        //    SPList VendorsList = SPContext.Current.Web.GetList(listUrl);
        //    ddlVendor.DataSource = VendorsList.Items.GetDataTable();
        //    ddlVendor.DataValueField = "ID";
        //    ddlVendor.DataTextField = "Title";
        //    ddlVendor.DataBind();
        //    ddlVendor.Items.Insert(0, new ListItem("All Vendors", "0"));
        //}

        //protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    FillDiscountedItems();
        //}

        //protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    FillDiscountedItems();
        //}
    }
}
